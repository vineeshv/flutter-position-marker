import 'package:custompainterdemo/home/display_image.dart';
import 'package:custompainterdemo/utils/image_resources.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  double height = 200;
  double width = 300;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Column(
        children: [
          DisplayImageComponent(
            imageUrl: ImageResources.imageUrl,
            height: height,
            width: width,
          ),
        ],
      ),
    );
  }
}
