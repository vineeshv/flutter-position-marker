import 'dart:ui';

import 'package:flutter/material.dart';

class SelectionPointer extends CustomPainter {
  Offset position;
  double pointRadius;
  Color color;

  SelectionPointer(
    this.position, {
    this.pointRadius = 10,
    this.color = Colors.red,
  });

  @override
  void paint(Canvas canvas, Size size) {
    const pointMode = PointMode.points;

    final paint = Paint()
      ..color = color
      ..strokeWidth = pointRadius
      ..strokeCap = StrokeCap.round;

    canvas.drawPoints(pointMode, [position], paint);
  }

  @override
  bool shouldRepaint(CustomPainter old) => true;
}
