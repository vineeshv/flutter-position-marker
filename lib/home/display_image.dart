import 'dart:ui';

import 'package:custompainterdemo/home/custom-painter/selection_pointer.dart';
import 'package:custompainterdemo/utils/points_handler.dart';
import 'package:flutter/material.dart';

class DisplayImageComponent extends StatefulWidget {
  final String imageUrl;
  final double height;
  final double width;

  const DisplayImageComponent({this.imageUrl, this.height, this.width});

  @override
  _DisplayImageComponentState createState() => _DisplayImageComponentState();
}

class _DisplayImageComponentState extends State<DisplayImageComponent> {
  PointsHandler _markPoints;

  @override
  void initState() {
    super.initState();
    _markPoints = PointsHandler(height: widget.height, width: widget.width);
  }

  void _onImagePositionSelected(TapDownDetails details) {
    setState(() {
      _markPoints.addNewPoint(Offset(
        details.localPosition.dx,
        details.localPosition.dy,
      ));
    });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTapDown: _onImagePositionSelected,
      child: Stack(
        children: [
          Image.network(
            widget.imageUrl,
            height: widget.height,
            width: widget.width,
            fit: BoxFit.fill,
          ),
          ...displaySelectedPoints()
        ],
      ),
    );
  }

  List<Widget> displaySelectedPoints() {
    return _markPoints.getSelectedPoints.map((pos) {
      return CustomPaint(
        painter: SelectionPointer(
          pos,
          pointRadius: _markPoints.pointRadius,
          color: _markPoints.color,
        ),
      );
    }).toList();
  }
}
