import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';

class PointsHandler {
  List<Offset> _selectedPoints;
  final double pointRadius;
  final double touchRadius;
  final Color color;
  final bool canEdit;
  final double height;
  final double width;

  PointsHandler({
    List<Offset> selectedPoints,
    this.height = 500,
    this.width = 500,
    this.pointRadius = 15,
    this.touchRadius = 35,
    this.color = Colors.red,
    this.canEdit = true,
  }) {
    if (selectedPoints != null) {
      _selectedPoints = selectedPoints
          .map((e) => Offset((e.dx * width) / 100, (e.dy * height) / 100))
          .toList();
    } else {
      _selectedPoints = [];
    }
  }

  List<Offset> get getSelectedPoints => _selectedPoints;

  void addNewPoint(Offset newPos) {
    if (!canEdit) return;

    final element = _selectedPoints.where((pos) {
      final dist = sqrt(
        pow(pos.dx - newPos.dx, 2) + pow(pos.dy - newPos.dy, 2),
      );

      return dist <= touchRadius;
    });

    if (element.isEmpty) {
      _selectedPoints.add(newPos);
    } else {
      _selectedPoints.remove(element.first);
    }
  }

  List<Offset> get getNormalizedValues {
    return _selectedPoints
        .map((e) => Offset((e.dx * 100) / width, (e.dy * 100) / height))
        .toList();
  }
}
